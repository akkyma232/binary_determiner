import React from 'react';
import { Link } from 'react-router-dom';
import SearchForm from './SearchForm.js';
import './TaxonSelect.css';

class TaxonSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
        }

        this.fetch_data = this.fetch_data.bind(this);
    }

    fetch_data() {
        fetch(`/api/determiner/taxon_search/${this.props.match.params.request}`)
        .then(response => response.json())
        .then(data => this.setState({data: data}))
    }

    componentDidMount() {
        this.fetch_data();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.match != prevProps.match) {
            if (this.props.match.params.request) {
                this.fetch_data();
            } else {
                this.setState({ 'data': [] });
            }
        }
        return prevProps, prevState;
    }

    render() {
        const taxons_list = this.state.data.map((item, index) =>
            <TaxonItem key={item['id']} taxonID={item['id']} even={index%2 == 0} name={item['name']}
                latin_name={item['latin_name']} rank={item['rank']} determiner={item['determiner_root']} has_determiner={item['has_determiner']}
            />);

        return <div className="taxon-select-main">
            <SearchForm text={this.state.request}/>
            <ul className="taxon-select-list">{taxons_list}</ul>
        </div>
    }
}

class TaxonItem extends React.Component {
    render() {
        var className = "taxon-select-item";
        if (this.props.even) {
            className += " item-even";
        } else {
            className += " item-odd";
        }

        const choice_button = this.props.determiner ?
        <Link to={`/choice/${this.props.determiner}`}>
            <button className="taxon-select-item-button">
                <i className="fa fa-arrow-right"/>
            </button>
        </Link>
        : "";

        const english_string = this.props.latin_name;
        const russian_string = this.props.name != "" ? "("+this.props.name+")" : "";

        return <li className={className}>
            <p><strong>{this.props.rank}</strong> {english_string} {russian_string}</p>
            <div className="taxon-select-item-button-container">
                { choice_button }
                <Link to={`/taxon/${this.props.taxonID}`}>
                    <button className="taxon-select-item-button"><i className="fa fa-file-text"></i></button>
                </Link>
            </div>
        </li>
    }
}

export default TaxonSelect
