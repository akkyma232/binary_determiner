import Modal from 'react-modal'
import React from 'react'
import './ImageViewer.css'
import 'font-awesome/css/font-awesome.css'
import image from "./hr.jpg"

Modal.setAppElement("#root");

class Viewer extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            isOpen: false,
            data: [],
            currentImageNumber: 0
        };
        if (!this.className) {
            this.className = ""
        }
        this.open = this.open.bind(this);
        this.close = this.close.bind(this);
        this.imageLeft = this.imageLeft.bind(this);
        this.imageRight = this.imageRight.bind(this);
        this.keyPressHandler = this.keyPressHandler.bind(this)
    }

    loadImages() {
        fetch(this.props.src)
            .then(response => response.json())
            .then(data => this.setState({data: data}));
    }

    componentDidMount() {
        document.addEventListener("keydown", this.keyPressHandler, false);
        this.componentDidUpdate({});
    }

    componentDidUpdate(prevProps) {
        if (this.props.src != prevProps.src) {
            if (this.props.src) {
                this.loadImages();
            } else {
                this.setState({data: []});
            }
        }
    }

    // componentWillUnmount() {
    //     document.removeEventListener("keydown");
    // }

    open(imageNumber) {
        this.setState({isOpen: true, currentImageNumber: imageNumber});
    }

    close() {
        this.setState({isOpen: false});
    }

    keyPressHandler(event) {
        if(event.keyCode == 37) {
            this.imageLeft()
        } else if (event.keyCode == 39) {
            this.imageRight()
        }
    }

    imageLeft() {
        this.imageShift(-1);
    }

    imageRight() {
        this.imageShift(+1);
    }

    imageShift(shift) {
        const maxNumber = this.state.data.length
        var newNumber = this.state.currentImageNumber + shift
        if (newNumber >= maxNumber) {
            newNumber %= maxNumber;
        } else if (newNumber < 0) {
            newNumber = (maxNumber + newNumber) % maxNumber
        }
        this.setState({currentImageNumber: newNumber})
    }

    render() {
        const { data } = this.state;

        var modal
        if (this.state.data.length > 0) {
            const current = this.state.data[this.state.currentImageNumber];
            modal =
            <Modal
                className="photosetModal"
                overlayClassName="photosetModalOverlay"
                isOpen={this.state.isOpen}
                onRequestClose={this.close}
            >
                <div className="photosetModalMain">
                    <div className="photosetModalImage">
                        <img src={current.photo} alt="insect" style={{maxWidth: "100%", maxHeight: "100%"}}/>
                        <div className="switchContainer">
                            <button className="switchButton" onClick={this.imageLeft}><i className="fa fa-chevron-left"></i></button>
                            <button className="switchButton" onClick={this.imageRight}><i className="fa fa-chevron-right"></i></button>
                        </div>
                    </div>
                    <div className="photosetModalText" id="style2">
                        <h4 className="photosetModalTitle">{current.title}</h4>
                        <p className="photosetModalDesc">{current.description}</p>
                    </div>
                </div>
            </Modal>
        }

        // return <div style={{width: '100%', height: '100%'}}>
        return <div className={"photosetContainer " + this.props.className}>
                { data.map((photoinfo, index) => (
                    <img className="photosetImage" key={index} onClick={() => this.open(index)} src={photoinfo.photo_small} alt="insect"/>
                ))}
                {modal}
            </div>
        // </div>
    }
}

export default Viewer;
