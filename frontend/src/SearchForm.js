import React from 'react';
import { Link } from 'react-router-dom';
import "font-awesome/css/font-awesome.css";
import './SearchForm.css';

class SearchForm extends React.Component {

    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getLinkTo = this.getLinkTo.bind(this);
    }

    componentDidMount() {
        this.input.value = this.props.text ? this.props.text : "";
        this.input.focus();
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.on_submit(this.input.value);
    }

    getLinkTo(location) {
        return `/search/${this.input ? this.input.value : ""}`;
    }

    render() {
        return <form className="search-form" onSubmit={this.handleSubmit}>
            <input type="text" className="search-input" placeholder={this.props.placeholder} ref={(input) => this.input=input} name="name" align='center'/>
            <Link to={this.getLinkTo}>
                <button className="search-button"><i className="fa fa-arrow-right"></i></button>
            </Link>
        </form>
    }
}

export default SearchForm;
