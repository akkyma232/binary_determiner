import React from 'react'
import Viewer from './ImageViewer.js'
import './Taxon.css'

class Taxon extends React.Component {
    constructor(props) {
        super(props)
        if (!this.className) {
            this.className = "";
        }

        this.state={
            data: this.props.taxon ? this.props.taxon : this.getEmptyTaxon(),
        }
    }

    componentDidMount() {
        fetch('/api/taxonomy/taxon/'+this.props.match.params.id)
            .then(response => response.json())
            .then(data => this.setState({data: data}));
    }

    componentDidUpdate(prevProps) {
        if (this.props.id != prevProps.id) {
            this.componentDidMount();
        }
    }

    getEmptyTaxon() {
        return {
            "name": "",
            "rank": "",
            "description": "",
        }
    }

    render() {
        const taxon = this.state.data;
        const viewer = taxon.photoset ? <Viewer className="taxon-viewer" src={"/api/" + taxon.photoset}></Viewer> : "";
        const name_string = taxon.latin_name ? 
            (taxon.latin_name + (taxon.name ? "("+taxon.name+")" : ""))
            : "";
        return <div className={"taxon-main " + this.className}>
            <div className="taxon-text">
                <h2>{taxon.rank + " " + name_string}</h2>
                <p>{taxon.description}</p>
            </div>
            {viewer}
        </div>
    }
}

export default Taxon;
