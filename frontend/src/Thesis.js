import React from 'react'
import { Link } from 'react-router-dom'
import Viewer from './ImageViewer.js'
import './Thesis.css'
import HeadShake from 'react-reveal/HeadShake';

class Thesis extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            text: "",
            next_taxon: null,
            photoset_link: null,
        };

        this.loadThesis = this.loadThesis.bind(this);
    }

    loadThesis() {
        this.setState({'text': null});
        fetch("/api/determiner/thesis/" + this.props.id)
            .then(response => response.json())
            .then(data => this.setState({text: data.text, next_taxon: data.next_taxon, photoset_link: data.photoset}))
    }

    componentDidUpdate(prevProps) {
        if (this.props.id != prevProps.id) {
            if (this.props.id) {
                this.loadThesis();
            } else {
                this.setState({
                    text: null,
                    next_taxon:  null,
                    photoset_link: null,
                });
            }
        }
    }

    componentDidMount() {
        this.componentDidUpdate({});
    }

    render() {
        const linkTo = this.state.next_taxon ?
            `/determiner/${this.state.next_taxon}` : `/choice/${this.props.id}`

        var component;
        if (this.state.text) {
            component = <HeadShake enter={false}>
            <div className="container">
                <p className="thesis-text">{this.state.text}</p>
                <div style={{width: "100%"}}>
                    {this.state.photoset_link ? <Viewer src={`/api/${this.state.photoset_link}`} className="thesis-photoset"/> : ""}
                    <Link to={linkTo} className='link'>
                        <button className='next-button'>
                            <i className="fa fa-arrow-down"></i>
                        </button>
                    </Link>
                </div>
            </div>
            </HeadShake>
        } else {
            component = <div></div>
        }

        return component
    }
}

export default Thesis