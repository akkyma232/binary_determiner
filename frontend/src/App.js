import React from 'react';
import { Switch, Route, Link, Redirect } from 'react-router-dom';
import './App.css';
import Choice from './Choice.js'
import SearchForm from './SearchForm.js'
import TaxonSelect from './TaxonSelect.js'
import Taxon from './Taxon'
import CustomLink from './CustomLink.js'


class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            choice: null,
            search_request: this.props.search_text,
            current_taxon: null,
        };
    }

    render() {
        const request = this.state.search_request;

        return <div id="Main">
            <Switch>
            <Route exact path="/">
                <div style={{width: '40%'}}>
                    <SearchForm placeholder="Клопы"/>
                </div>
            </Route>
            <Route path="/">
            <CustomLink tag='div' id="App-main" to="/search">
                <Switch>
                <Route exact path="/search/">
                    <Redirect to="/search/Клопы"/>
                </Route>
                <Route path="/search/:request" 
                    component={TaxonSelect}
                />
                <Route path="/taxon/:id"
                    component={Taxon}
                />
                <Route path={["/determiner/:id", "/choice/:id"]}
                    component={Choice}
                />
                </Switch>
            </CustomLink>
            </Route>
            </Switch>
        </div>
    }
}

export default App;
