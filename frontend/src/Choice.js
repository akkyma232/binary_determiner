import React from 'react'
import { Redirect } from 'react-router-dom';
import Viewer from './ImageViewer.js'
import './Choice.css'
import Thesis from "./Thesis.js"

class Taxon extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
        }

        this.expandTaxon = this.expandTaxon.bind(this);
        this.loadTaxon = this.loadTaxon.bind(this);

    }
    expandTaxon() {
        const taxon_div = document.getElementById('taxon-container');
        taxon_div.classList.toggle('expanded');
        const taxon_photoset = document.getElementById('taxon-photoset');
        taxon_photoset.classList.toggle('expanded');
    }

    loadTaxon() {
        fetch('/api/taxonomy/taxon/'+this.props.id)
        .then(response => response.json())
        .then(data => this.setState({data: data}))
    }

    componentDidMount() {
        this.componentDidUpdate({id: null});
    }

    componentDidUpdate(prevProps) {
        if (this.props.id != prevProps.id) {
            this.loadTaxon();
        }
    }

    render() {
        const data = this.state.data || {};
        return <div id='taxon-container' className='taxon-container' onClick={this.expandTaxon}>
            <div className='taxon-text'>
                <h2>{data.name || ""}</h2>
                <p>{data.description || ""}</p>
            </div>
            <div id='taxon-photoset'>
                <Viewer className="taxon-viewer" src={`/api/${data.photoset}`}/>
            </div>
        </div>
    }
}

class Choice extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            data: this.getEmptyData(),
            redirect_to: null,
            taxon_id: null,
            choice_id: null,
            thesis1_id: null,
            thesis2_id: null,
        };

        this.loadDeterminer = this.loadDeterminer.bind(this);
        this.loadTaxon = this.loadTaxon.bind(this);
        this.loadChoice = this.loadChoice.bind(this);
    }

    getEmptyData() {
        return {
            'name': "",
            'description': "",
        }
    }

    loadDeterminer(id) {
        fetch(`/api/determiner/taxon_start/${id}`)
        .then((response) => {
            if (response.status != 404 && response.status != 500) {
                return response.json();
            } else {
                this.setState({ 'redirect_to': `/taxon/${id}` })
                return Promise.reject();
            }
        })
        .then(data => this.setState({taxon_id: id, choice_id: data.id}))
    }

    loadTaxon() {
        fetch('/api/taxonomy/taxon/'+this.state.taxon_id)
        .then(response => response.json())
        .then(data => this.setState({data: data}))
    }

    loadChoice() {
        fetch('/api/determiner/thesis/' + this.state.choice_id)
            .then(response => response.json())
            .then(data => this.setState({taxon_id: data.determiner, thesis1_id: data.child_theses[0], thesis2_id: data.child_theses[1]}))
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.match.url != prevProps.match.url) {
            if (this.props.match.url.startsWith("/determiner/")) {
                this.loadDeterminer(this.props.match.params.id);
            } else if (this.props.match.url.startsWith("/choice/")) {
                this.setState({
                    choice_id: this.props.match.params.id
                });
            } else {
                console.error("Wrong url for component Choice: ", this.props.match.url);
            }
        }
        
        if (this.state.taxon_id != prevState.taxon_id) {
            if (this.state.taxon_id) {
                this.loadTaxon();
            } else {
                this.setState({data: this.getEmptyData()});
            }
        }
        if (this.state.choice_id != prevState.choice_id) {
            if (this.state.choice_id) {
                this.loadChoice();
            } else {
                this.setState({thesis1_id: null, thesis2_id: null});
            }
        }
    }

    componentDidMount() {
        this.componentDidUpdate({'match': {'params': {'url': null}}}, {taxon_id: null, choice_id: null});
    }

    render() {
        if (this.state.redirect_to) {
            return <Redirect to={this.state.redirect_to}/>
        }

        return <div className='container'>
            <div className='thesis-container'>
                <Thesis id={this.state.thesis1_id} />
            </div>

            <Taxon id={this.state.taxon_id}/>
            
            <div className='thesis-container'>
                <Thesis id={this.state.thesis2_id} />
            </div>
        </div>
    }
}

export default Choice
