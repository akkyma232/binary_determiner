from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import TaxonDeterminer, Thesis

@receiver(post_save, sender=TaxonDeterminer)
def echo(sender, instance, created, raw, using, update_fields, **kwargs):
    if created:
        root_thesis = Thesis(determiner=instance)
        root_thesis.save()
        instance.root = root_thesis
        instance.save()