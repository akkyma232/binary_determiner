from django.contrib import admin
from django.contrib.admin.options import TO_FIELD_VAR, IS_POPUP_VAR, csrf_protect_m
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import redirect

from .models import TaxonDeterminer, Thesis, ThesisPhotoInfo


class ThesisInline(admin.TabularInline):
    model = Thesis
    fields = ['parent_thesis']
    extra = 0


@admin.register(TaxonDeterminer)
class ChoiceGraphAdmin(admin.ModelAdmin):   
    model = TaxonDeterminer
    inlines = [ThesisInline]
    exclude = ['root']
    add_form_template = ""
    change_form_template = "admin/determiner/taxondeterminer/change_form.html"

    def get_formsets_with_inlines(self, request, obj=None):
        for inline in self.get_inline_instances(request, obj):
            # hide ThesisInline in the add view
            if not isinstance(inline, ThesisInline) or obj is not None:
                yield inline.get_formset(request, obj), inline

    def save_model(self, request, obj, form, change):
        super().save_model(request, obj, form, change)


class PhotoInfoInline(admin.TabularInline):
    model = ThesisPhotoInfo
    fields = ('title', 'description', 'photo')


@admin.register(Thesis)
class ThesisAdmin(admin.ModelAdmin):
    model = Thesis
    fields = ['determiner', 'text', 'next_taxon']
    inlines = [PhotoInfoInline]

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}

    def has_change_permission(self, request, obj=None):
        if obj is not None and obj.is_root():
            return False
        return super().has_change_permission(request, obj=obj)

    def has_delete_permission(self, request, obj=None):
        if obj is not None and obj.is_root() and request.path.endswith("change/"):
            return False

        return True

    def response_add(self, request, obj):
        return redirect(f"/admin/determiner/taxondeterminer/{obj.determiner_id}")

    def response_change(self, request, obj):
        return redirect(f"/admin/determiner/taxondeterminer/{obj.determiner_id}")
