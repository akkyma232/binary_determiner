from django.urls import include, path
from django.conf.urls import url
from rest_framework.generics import RetrieveAPIView

from .views import *
from .models import *
from .serializers import *


urlpatterns = [
    path('taxon_start/<int:pk>', taxon_start),
    path('taxon_search/<str:name>', TaxonSearch.as_view()),
    path('thesis/<int:pk>', RetrieveAPIView.as_view(queryset=Thesis.objects.all(), serializer_class=ThesisSerializer)),
    path('photoset/<int:pk>', PhotoInfoListView.as_view()),
]
