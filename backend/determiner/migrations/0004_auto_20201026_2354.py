# Generated by Django 2.2.9 on 2020-10-26 23:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('determiner', '0003_thesis_parent_thesis'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thesis',
            name='parent_thesis',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='child_theses', to='determiner.Thesis'),
        ),
        migrations.AlterField(
            model_name='thesis',
            name='text',
            field=models.TextField(null=True, verbose_name='текст'),
        ),
    ]
