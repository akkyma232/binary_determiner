from rest_framework import serializers
from binarydeterminer.serializers import PhotoInfoSerializer
from taxonomy.serializers import TaxonMinSerializer as BaseTaxonMinSerializer
from .models import Thesis, ThesisPhotoInfo


class TaxonMinSerializer(BaseTaxonMinSerializer):
    determiner_root = serializers.SlugRelatedField(source='determiner', slug_field='root_id', read_only=True)
    # has_determiner = serializers.SerializerMethodField('get_has_determiner')

    def get_has_determiner(self, model):
        return hasattr(model, 'determiner')

    class Meta(BaseTaxonMinSerializer.Meta):
        fields = BaseTaxonMinSerializer.Meta.fields + ['determiner_root']

class ThesisIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Thesis
        fields = ['id']

class ThesisSerializer(serializers.ModelSerializer):
    photoset = serializers.SerializerMethodField('get_photoset_link')

    def get_photoset_link(self, thesis):
        return f"determiner/photoset/{thesis.id}"

    class Meta:
        model = Thesis
        fields = ['text', 'determiner', 'photoset', 'next_taxon', 'child_theses']

class ThesisPhotoInfoSerializer(PhotoInfoSerializer):
    class Meta(PhotoInfoSerializer.Meta):
        model = ThesisPhotoInfo
