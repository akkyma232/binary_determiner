from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework import generics
import taxonomy.views
from taxonomy.models import Taxon
from .models import TaxonDeterminer, Thesis
from .serializers import *


class TaxonSearch(taxonomy.views.TaxonSearch):
    serializer_class = TaxonMinSerializer

@api_view(['GET'])
def taxon_start(request, pk):
    taxon = Taxon.objects.get(pk=pk)
    # if not hasattr(taxon, "choice_set"):
    #     return Response(status=status.HTTP_404_NOT_FOUND)
    # choices = taxon.choice_set.choices.filter(parent_thesis=None)
    # if len(choices) == 0:
    #     return Response(status=status.HTTP_404_NOT_FOUND)
    # choice = choices.first()
    determiner = taxon.determiner
    if determiner is not None:
        thesis = determiner.root
        serializer = ThesisIdSerializer(thesis)
        return Response(serializer.data)

class PhotoInfoListView(generics.ListAPIView):
    serializer_class = ThesisPhotoInfoSerializer

    def get_queryset(self):
        photoset_id = self.kwargs['pk']
        queryset = ThesisPhotoInfo.objects.filter(model=photoset_id)
        return queryset
