from django.db import models
from django.core.exceptions import ValidationError
from taxonomy.models import Taxon
from binarydeterminer.models import PhotoInfo


class TaxonDeterminer(models.Model):
    taxon = models.OneToOneField(Taxon, null=True, blank=False, on_delete=models.SET_NULL, verbose_name="Текущий таксон", related_name="determiner")
    root = models.OneToOneField('Thesis', null=True, on_delete=models.PROTECT)

    def __str__(self):
        return self.taxon.__str__()

class Thesis(models.Model):
    text = models.TextField(verbose_name="текст", null=True)
    determiner = models.ForeignKey(TaxonDeterminer, on_delete=models.CASCADE, related_name="theses")
    parent_thesis = models.ForeignKey('Thesis', on_delete=models.SET_NULL, null=True, blank=True, related_name='child_theses')
    next_taxon = models.ForeignKey(Taxon, on_delete=models.SET_NULL, null=True, blank=True,
                                   verbose_name="Следующий таксон")

    def __str__(self):
        if self.is_root():
            return "Root"
        elif self.text:
            return self.text[:20]
        else:
            return "None"

    def is_root(self):
        if self.text is None:
            return True

    class Meta:
        verbose_name = "Тезис"
        verbose_name_plural = "Тезисы"

class ThesisPhotoInfo(PhotoInfo):
    model = models.ForeignKey(Thesis, on_delete=models.CASCADE, related_name='photoinfo_set')
