from django.apps import AppConfig


class DeterminerConfig(AppConfig):
    name = 'determiner'

    def ready(self):
        import determiner.signals
