from rest_framework import serializers
from binarydeterminer.serializers import PhotoInfoSerializer
from .models import *


class TaxonSerializer(serializers.ModelSerializer):
    rank = serializers.SlugRelatedField(slug_field='name', read_only=True)
    photoset = serializers.SerializerMethodField('get_photoset_link')

    def get_photoset_link(self, taxon):
        return f"taxonomy/photoset/{taxon.id}"

    class Meta:
        model = Taxon
        fields = ['id', 'name', 'latin_name', 'description', 'rank', 'parent_taxon', 'photoset']


class TaxonMinSerializer(serializers.ModelSerializer):
    rank = serializers.SlugRelatedField(slug_field='name', read_only=True)

    class Meta:
        model = Taxon
        fields = ['id', 'name', 'latin_name', 'rank']

class TaxonPhotoInfoSerializer(PhotoInfoSerializer):
    class Meta(PhotoInfoSerializer.Meta):
        model = TaxonPhotoInfo
