from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.utils.translation import ugettext_lazy as _
from orderable.admin import OrderableAdmin
from .models import TaxonRank, Taxon, TaxonPhotoInfo
from .filters import TaxonHasImageFilter, TaxonHasDescriptionFilter, TaxonHasParentFilter


class TaxonRankAdmin(OrderableAdmin):
    exclude = ('sort_order',)
    list_display = ('name', 'sort_order_display')


class PhotoInfoInline(admin.TabularInline):
    model = TaxonPhotoInfo
    fields = ('title', 'description', 'photo')


class TaxonAdmin(ModelAdmin):
    sortable_by = ('rank',)
    ordering = ('rank',)
    list_display = ('full_name', 'rank', 'parent_taxon', 'photo_count')
    list_filter = ('rank', TaxonHasParentFilter, TaxonHasDescriptionFilter, TaxonHasImageFilter)
    fields = ('rank', 'parent_taxon', 'name', 'latin_name', 'description', 'image', 'image_tag',)
    readonly_fields = ('image_tag',)
    inlines = (PhotoInfoInline,)


admin.site.register(TaxonRank, TaxonRankAdmin)
admin.site.register(Taxon, TaxonAdmin)
