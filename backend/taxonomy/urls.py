from django.urls import include, path
from rest_framework import routers
from .models import *
from .views import *
from .serializers import *
from rest_framework.generics import RetrieveAPIView, ListAPIView


urlpatterns = [
    path('taxon/<int:pk>', RetrieveAPIView.as_view(queryset=Taxon.objects.all(), serializer_class=TaxonSerializer)),
    path('taxon_search/<str:name>', TaxonSearch.as_view()),
    path('photoset/<int:pk>', PhotoInfoListView.as_view()),
]
