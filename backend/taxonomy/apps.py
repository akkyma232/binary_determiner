from django.apps import AppConfig


class TaxonomyConfig(AppConfig):
    name = 'taxonomy'
    verbose_name = "Таксономия"
