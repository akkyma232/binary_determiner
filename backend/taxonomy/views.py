from rest_framework import viewsets
from rest_framework import views
from rest_framework import generics
from .models import *
from .serializers import *
from rest_framework.response import Response


class TaxonSearch(generics.ListAPIView):
    serializer_class = TaxonMinSerializer

    def get_queryset(self):
        name = self.kwargs['name']

        if name == "":
            return []

        words = name.split()

        rank_name = words[0]
        result1 = Taxon.objects.filter(rank__name__trigram_similar=rank_name)
        if len(words) > 1:
            taxon_name = " ".join(words[1:])
            result1 = result1.filter(name__trigram_similar=taxon_name)

        result2 = Taxon.objects.filter(name__trigram_similar=name)

        result3 = Taxon.objects.filter(latin_name__trigram_similar=name)

        result = result1 | result2 | result3

        return result


class PhotoInfoListView(generics.ListAPIView):
    serializer_class = TaxonPhotoInfoSerializer

    def get_queryset(self):
        photoset_id = self.kwargs['pk']
        queryset = TaxonPhotoInfo.objects.filter(model=photoset_id)
        return queryset
