from django.db import migrations, models
from django.contrib.postgres.operations import TrigramExtension

class Migration(migrations.Migration):

    dependencies = [('taxonomy', '0001_initial')]

    operations = [
        TrigramExtension()
    ]
