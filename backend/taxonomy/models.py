from django.db import models
from django.db.models import Model
from django.core.exceptions import ValidationError
from orderable.models import Orderable
from binarydeterminer.models import PhotoInfo
from django.utils.safestring import mark_safe


class TaxonRank(Orderable):
    name = models.CharField(max_length=30, verbose_name="название")

    def __str__(self):
        return self.name

    class Meta(Orderable.Meta):
        verbose_name = "Ранг таксона"
        verbose_name_plural = "Ранги таксонов"


class Taxon(Model):
    rank = models.ForeignKey(TaxonRank, on_delete=models.PROTECT, verbose_name="ранг")
    parent_taxon = models.ForeignKey('Taxon', on_delete=models.SET_NULL, null=True, blank=True,
                                     related_name='child_taxons', verbose_name="родительский таксон")
    name = models.CharField(max_length=50, null=True, blank=True, verbose_name="русское название")
    latin_name = models.CharField(max_length=50, verbose_name="латинское название")
    description = models.TextField(default="", blank=True, verbose_name="описание")

    def full_name(self):
        if self.name: 
            return f"{self.latin_name}({self.name})"
        else:
            return f"{self.latin_name}"
    full_name.short_description = "Название"

    def image_tag(self):
        return mark_safe('<img src="%s" style="max-width: 50em; max-height: 30em"/>' % self.image.url)
    image_tag.short_description = 'обзор'
    def image_path(self, filename):
        return f"taxon_images/{self.name}"
    image = models.ImageField(upload_to=image_path, null=True, blank=True, verbose_name="изображение")

    def photo_count(self):
        return self.photoinfo_set.count()
    photo_count.short_description = "Количество фотографий"

    def clean(self):
        if self.parent_taxon == self:
            raise ValidationError("Таксон не может быть родителем самому себе")
        if self.parent_taxon is not None and self.parent_taxon.rank.sort_order >= self.rank.sort_order:
            raise ValidationError("Ранг дочернего таксона не может быть выше или равно рангу родительского таксона")

    def __str__(self):
        return f"{self.rank} {self.latin_name}"

    class Meta:
        verbose_name = "таксон"
        verbose_name_plural = "таксоны"


class TaxonPhotoInfo(PhotoInfo):
    model = models.ForeignKey(Taxon, on_delete=models.CASCADE, related_name='photoinfo_set')
