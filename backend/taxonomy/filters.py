from django.contrib import admin


class TaxonHasParentFilter(admin.SimpleListFilter):
    title = 'родитель'
    parameter_name = 'parent'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Есть'),
            ('No', 'Нет'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            return queryset.exclude(parent_taxon=None)
        elif value == 'No':
            return queryset.filter(parent_taxon=None)
        return queryset


class TaxonHasDescriptionFilter(admin.SimpleListFilter):
    title = 'описание'
    parameter_name = 'description'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Есть'),
            ('No', 'Нет'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            return queryset.exclude(description='')
        elif value == 'No':
            return queryset.filter(description='')
        return queryset


class TaxonHasImageFilter(admin.SimpleListFilter):
    title = 'изображение'
    parameter_name = 'image'

    def lookups(self, request, model_admin):
        return (
            ('Yes', 'Есть'),
            ('No', 'Нет'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'Yes':
            return queryset.exclude(photo='')
        elif value == 'No':
            return queryset.filter(photo='')
        return queryset
