from django.db import models
from django.utils.safestring import mark_safe
from imagekit.models.fields import ImageSpecField
from imagekit.processors import ResizeToFit


class PhotoInfo(models.Model):
    title = models.CharField(max_length=60, verbose_name="заголовок", blank=True)
    description = models.TextField(verbose_name="текст", blank=True)

    def photo_tag(self):
        return mark_safe('<img src="%s" style="max-width: 30em; max-height: 20em"/>' % self.photo.url)
    photo_tag.short_description = "Превью"

    def photo_path(self):
        pass

    photo = models.ImageField(upload_to="photoinfo", verbose_name="фото", blank=False, null=False)
    photo_small = ImageSpecField(processors=[ResizeToFit(128, 128)], source="photo", format="JPEG")

    class Meta:
        abstract = True
        verbose_name = "фотоинфо"
        verbose_name_plural = "фотоинфо"
