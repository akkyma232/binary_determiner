from rest_framework import serializers
from .models import *

class PhotoInfoSerializer(serializers.HyperlinkedModelSerializer):
    photo = serializers.ReadOnlyField(source="photo.url")
    photo_small = serializers.ReadOnlyField(source="photo_small.url")

    class Meta:
        fields = ["title", "description", "photo", "photo_small"]
